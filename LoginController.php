<?php

namespace Ksum\Oauth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ksum\Oauth\LoginHelper;
use Ksum\Oauth\UidHelper;

class LoginController extends Controller
{
    public function login($type = null){
        if($type == "startup"){
            return UidHelper::login();
        }
        elseif($type == "ksum"){
            return LoginHelper::login(request()->input('register_url'), $type);
        }
        else{
            return view('auth.login');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return LoginHelper::logout($this->logoutRedirect());
    }

    public function logoutRedirect(){
        return null;
    }
}
