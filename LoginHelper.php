<?php

namespace Ksum\Oauth;

use Illuminate\Support\Facades\Http;

class LoginHelper{

  public static function login($register_url = null, $type = null){
    $query = http_build_query([
      'client_id' => config('oauth.id'),
      'redirect_uri' => route(config('oauth.callback')),
      'response_type' => 'code',
      'register_url' => $register_url,
      'state' => $type,
    ]);

    return redirect(config('oauth.server').'/oauth/authorize?'.$query);
  }

  public static function callback(){

    $response = Http::asForm()->post(config('oauth.server').'/oauth/token', [
        'grant_type' => 'authorization_code',
        'client_id' => config('oauth.id'),
        'client_secret' => config('oauth.secret'),
        'redirect_uri' => route(config('oauth.callback')),
        'code' => request()->code,
    ]);

    $data = $response->json();

    if(array_key_exists('access_token', $data)){

      $response = Http::withToken($data['access_token'])->get(config('oauth.server').'/api/user');

      $data = $response->json();

      if(array_key_exists('email', $data)){
        return $data;
      }

    }

    return false;

  }

  public static function logout($redirect){
    $query = http_build_query([
      'redirect' => $redirect,
    ]);

    return redirect(config('oauth.server').'/logout?'.$query);
  }

}