<?php

namespace Ksum\Oauth;

use Illuminate\Support\ServiceProvider;

class OauthServiceProvider extends ServiceProvider{

    public function boot()
    {

      $this->publishes([
        __DIR__.'/config/oauth.php' => config_path('oauth.php'),
        __DIR__.'/config/uid.php' => config_path('uid.php'),
      ]);

      $this->loadRoutesFrom(__DIR__.'/routes.php');
      
    }
}