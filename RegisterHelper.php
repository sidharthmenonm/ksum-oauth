<?php

namespace Ksum\Oauth;

use Illuminate\Support\Facades\Http;

class RegisterHelper{
  
  public static function register($name, $email){
    $query = http_build_query([
      'name' => $name,
      'email' => $email,
    ]);

    $url = config('oauth.server').'/register/'.config('oauth.id').'/user?'.$query;

    $response = Http::get($url);

    return $response;
  }

}