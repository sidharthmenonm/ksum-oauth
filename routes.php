<?php
use App\Http\Controllers\Auth\LoginController;

Route::middleware('web')->group(function(){

  Route::middleware('guest')->get('login/{type?}', [LoginController::class, 'login'])->name('login');
  
  Route::get('oauth/callback', [LoginController::class, 'callback'])->name('oauth.callback');
  Route::get('oauth/startup/callback', [LoginController::class, 'startup_callback'])->name('oauth.startup.callback');

  Route::get('logout', [LoginController::class, 'logout'])->name('logout');

});